from cgitb import lookup
from pathlib import Path
import datetime
import json
from PIL import Image
import sys
import pandas as pd
from csv import reader
import logging


class DataLabeler:

    def __init__(self, blacklist=[], image_size=128):
        logging.basicConfig(format="%(asctime)-15s [%(levelname)s] %(funcName)s in %(filename)s: %(message)s", level=logging.INFO)
        self.dir = Path("data")
        if not self.dir.exists():
            self.dir.mkdir(parents=True)
        
        self.dataset_dir()
        self.create_whitelist(blacklist)    

        self.image_size = image_size

        ### Classification Classes ###
        self.road_types = [
            "asphalted",
            "paved",
            "unpaved"
        ]
        self.road_qualities = [
            "good",
            "medium",
            "bad"
        ]
        logging.info("created data labeler")



    def dataset_dir(self):
        """create and get dataset path"""
        data_dir = Path("data")
        number = len([dataset for dataset in data_dir.glob("dataset_*")])
        self.dataset_path = data_dir/"dataset_{:0>3}".format(number)
        
    def create_whitelist(self, blacklist=[]):
        """input blacklist output whitelist"""
        available_raw_data = [raw.name for raw in Path("data/raw").glob("*")]
        for ignored_raw_data in blacklist:
            if ignored_raw_data in available_raw_data:
                available_raw_data.remove(ignored_raw_data)
        self.whitelist = available_raw_data

    def create_subfolder(self):
        """create image and acceleration subfolder in dataset"""
        self.dataset_path.mkdir()
        (self.dataset_path / "images").mkdir()
        (self.dataset_path / "acceleration").mkdir()
        for type in self.road_types:
            (self.dataset_path / "images" / type).mkdir()



    def image_dir(self, raw_path):
        """get raw image data directory"""
        self.imagedir = raw_path/"images"
    
    def label_data_road_type(self, raw_path):
        """get label.json data for road type"""
        file_path = raw_path/"labels.json"
        with open(file_path, 'r') as fp:
            data = json.load(fp)
            self.json_lines_road_type = data["road_type"][:][:]
        
    def check_label_file(self):
        """check label.json file"""
        for json_line in self.json_lines_road_type:
            start_image = json_line[1]
            end_image = json_line[2]
            if end_image < start_image:
                logging.error("wrong label.json input")
                sys.exit()
        for json_line in self.json_lines_road_quality:
            start_image = json_line[1]
            end_image = json_line[2]
            if end_image < start_image:
                logging.error("wrong label.json input")
                sys.exit()
        logging.info("checked label file")
    
    def sort_crop_image_data(self):
        """sort image data into subfolder and crop images"""
        shift = -15

        for json_line in self.json_lines_road_type:
            type = json_line[0]
            start_image = json_line[1]
            end_image = json_line[2]

            copyintopath = self.dataset_path / "images" / self.road_types[type]

            for image_number in range(start_image, end_image+1):
                for last_number in range(0, 10):
                    imagepath = str(image_number) + "_{0:01d}".format(last_number) + ".jpg"
                    copyfrompath = Path(self.imagedir / imagepath)
                    if copyfrompath.exists():
                        im = Image.open(copyfrompath)
                        width, height = im.size
                        left = width/2 - self.image_size/2 + shift
                        new_height = (self.image_size + (1/8)*height)
                        top = height - new_height
                        right = width/2 + self.image_size/2 + shift
                        bottom = top + self.image_size
                        # Crop the center of the image
                        im_cropped = im.crop((left, top, right, bottom))
                        im_cropped.save(copyintopath/imagepath)
        logging.info("sorted image data")

    def label_data_road_quality(self, raw_path):
        """get label.json data for road quality"""
        file_path = raw_path/"labels.json"
        with open(file_path, 'r') as fp:
            data = json.load(fp)
            self.json_lines_road_quality = data["road_quality"][:][:]
            
    def create_acc_lst(self, raw_path):
        """create list with raw integer acceleration value"""
        self.accdir = raw_path/"accelerometer.csv"
        with open(self.accdir) as f:
            firstColumn = [line.split(',')[0] for line in f]
        lst = []
        for each in firstColumn:
            lst.append(str(each).split('.')[0])
        return lst
        
    def write_total_acc_recording_time(self, lst):
        """write total acceleration number and recording time into config.json"""
        total_acc = len(lst)
        recording_time = int(lst[-1]) - int(lst[1])
        self.total_acc_sum += total_acc
        self.recording_time_sum += recording_time
    
    def sort_acc_data(self, lst):
        """sort acceleration data"""
        for json_line in self.json_lines_road_quality:
            quality = json_line[0]
            start_acc = json_line[1]
            end_acc = json_line[2]

            csv_todir = self.dataset_path / "acceleration" / (self.road_qualities[quality] + ".csv")
            
            start_index=0
            end_index=0
            
            if str(start_acc) in lst:
                start_index=lst.index(str(start_acc))
                start_index=start_index-1

            if str(end_acc+1) in lst:
                end_index=lst.index(str(end_acc+1))
                end_index=end_index-2
            else:
                # last element in list
                end_index=len(lst)-1

            
            new = not Path(csv_todir).exists()
            df = pd.read_csv(self.accdir)
            df=df.loc[start_index:end_index,:]
            df.to_csv(csv_todir, index=False, mode='a', header=new)

        logging.info("sorted acceleration data")
    
    def write_gps_data(self, raw_path):
        """write gps.csv files from raw folder into dataset folder"""
        csv_fromdir = raw_path/"gps.csv"
        new = not Path(self.dataset_path / "gps.csv").exists()
        df = pd.read_csv(csv_fromdir)
        df.to_csv(self.dataset_path/"gps.csv", index=False, mode='a', header=new)
        logging.info("wrote gps data")

    def write_config_info(self):
        """write several information into config.json"""
        self.info = {
            "date": datetime.datetime.now().strftime("%d.%m.%Y"),
            "images": {
                "total": 0,
                "asphalted": None, 
                "paved": None, 
                "unpaved": None
            },
            "acceleration": {
                "total": 0,
                "good": None, 
                "medium": None, 
                "bad": None
            },
            "recording_time_s": self.recording_time_sum,
            "image_size": self.image_size,
            "raw_content": self.whitelist
        }

        for type in self.road_types:
            path = self.dataset_path / "images" / type
            number = len([image for image in path.glob("*")])
            self.info["images"][type] = number 
            self.info["images"]["total"] += number

        for quality in self.road_qualities:
            path = self.dataset_path / "acceleration" /( quality + ".csv")
            number = pd.read_csv(path).shape[0]
            self.info["acceleration"][quality] = number 
            self.info["acceleration"]["total"] += number
        
        with open(self.dataset_path/"config.json", "w") as outfile:
            json.dump(self.info, outfile, indent=4, sort_keys=False)

        logging.info("wrote config information")

    def run(self):
        logging.info("creating dataset " + self.dataset_path.name)
        self.create_subfolder()

        self.total_acc_sum = 0
        self.recording_time_sum = 0

        # iterate through multiple raw data folder
        for raw_number in self.whitelist:
            logging.info("working on folder " + raw_number)
            raw_path = Path("data/raw") / raw_number
            self.image_dir(raw_path)
            self.label_data_road_type(raw_path)
            self.label_data_road_quality(raw_path)
            self.check_label_file()
            self.sort_crop_image_data()
            lst = self.create_acc_lst(raw_path)
            self.write_total_acc_recording_time(lst)
            self.sort_acc_data(lst)
            self.write_gps_data(raw_path)

        self.write_config_info()
    
        

def main():
    self = DataLabeler(blacklist=["000_220222"])
    self.run()

if __name__=="__main__":
    main()