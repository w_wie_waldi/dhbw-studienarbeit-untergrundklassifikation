from components import Camera, GPS, Accelerometer, Menu, Item
import time, datetime
import cv2
import pandas as pd
from pathlib import Path
import threading, subprocess
import logging


class DataRecorder:
    def __init__(self):
        self.dir = Path("/studienarbeit/data/raw")
        if not self.dir.exists():
            self.dir.mkdir(0o777, parents=True)
        self.menu = Menu(
            title="Data Recorder",
            items=[
                Item("Record", self.start_record, self.stop_record),
                Item("Exit", self._exit),
                Item("Shutdown", self._shutdown)
            ]
        )
        self.recording = False
        logging.basicConfig(format="%(asctime)-15s [%(levelname)s] %(funcName)s in %(filename)s: %(message)s", level=logging.INFO)
        logging.info("Started DataRecorder")

    def _record_dir(self):
        """Determine the directory path for a new record."""
        date = datetime.datetime.now().strftime("%d%m%y")
        number = len([record for record in self.dir.glob("*")])
        dir = self.dir/"{:0>3}_{}".format(number, date)
        return dir

    def start_record(self):
        """Start a new record."""
        if not self.recording:
            self.recording = True
            self.record = Record(self._record_dir())
            self.record.start()
            self.menu.get_state = self.record.get_state

    def stop_record(self):
        """Stop the record."""
        if self.recording:
            self.record.stop()
            del self.record
            self.recording = False
            self.menu.get_state = None
            
    def _on_left(self):
        """Callback for encoder left turn."""
        self.position = (self.position - 1) % len(self.items)

    def _on_right(self):
        """Callback for encoder right turn."""
        self.position = (self.position + 1) % len(self.items)

    def run(self):
        """Function to call to run the program."""
        self.menu.run()

    def _exit(self):
        """Stop the python program."""
        self.menu.running = False

    def _shutdown(self):
        """Force the Jetson to shut down."""
        subprocess.run(["shutdown now -h"], shell=True)


class Record:

    def __init__(self, dir):
        """Record data to given directory."""
        dir.mkdir(0o777)
        self.image_dir = dir/"images"
        self.image_dir.mkdir(0o777)
        self.camera = Camera()
        self.gps_path = dir/"gps.csv"
        self.gps = GPS()
        self.accelerometer_path = dir/"accelerometer.csv"
        self.accelerometer = Accelerometer(sample_rate=1000)
        self.state = {"camera": False, "gps": False, "accelerometer": False}
        
    def start(self):
        """Start recording."""
        logging.info("Starting new Record")
        self.recording = True
        self.camera_thread = threading.Thread(target=self._record_camera)
        self.camera_thread.start()
        self.gps_thread = threading.Thread(target=self._record_gps)
        self.gps_thread.start()
        self.accelerometer_thread = threading.Thread(target=self._record_accelerometer)
        self.accelerometer_thread.start()

    def stop(self):
        """Stop recording."""
        logging.info("Stopping running Record")
        self.recording = False
        if self.camera_thread.is_alive():
            self.camera_thread.join()
        if self.gps_thread.is_alive():
            self.gps_thread.join()
        if self.accelerometer_thread.is_alive():
            self.accelerometer_thread.join()

    def get_state(self):
        return self.state

    def _image_path(self):
        """Create a new image path."""
        utc = round(time.time())
        number = len([image for image in self.image_dir.glob("{}_*".format(utc))])
        path = self.image_dir/"{}_{}.jpg".format(utc, number)
        return str(path)

    def _record_camera(self):
        """Camera recording thread."""
        self.state["camera"] = True
        try:
            self.camera.on()
            while self.recording:
                image = self.camera.read()
                cv2.imwrite(self._image_path(), image)
        except Exception as e:
            logging.error(e)
        finally:
            self.camera.off()
            self.state["camera"] = False
    
    def _record_gps(self):
        """GPS receiver recording thread."""
        self.state["gps"] = True
        try:
            self.gps.on()
            while self.recording:
                location = pd.DataFrame.from_dict(self.gps.read())
                new = not Path(self.gps_path).exists()
                location.to_csv(self.gps_path, mode="a", index=False, header=new)
        except Exception as e:
            logging.error(e)
        finally:
            self.gps.off()
            self.state["gps"] = False

    def _record_accelerometer(self):
        """Accelerometer recording thread."""
        self.state["accelerometer"] = True
        try:
            self.accelerometer.on()
            while self.recording:
                data_points = self.accelerometer.read()
                new = not Path(self.accelerometer_path).exists()
                data_points.to_csv(self.accelerometer_path, mode="a", index=False, header=new)
        except Exception as e:
            logging.error(e)
        finally:
            self.accelerometer.off()
            self.state["accelerometer"] = False


def main():
    recorder = DataRecorder()
    recorder.run()


if __name__=="__main__":
    main()