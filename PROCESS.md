# Processes #

## Database ##

    output/**/waypoints.csv: [way_id, lat*, lon*, type, quality, date]

        osm id search:
        - node_ids of way_id -> segments
        segment division (<10m):
        - segment -> subsegments
        - segment_id
        - length

    segments/segments.csv: [way_id, segment_id, lat_a, lon_a, lat_b, lon_b, length]


## Map ##

    raw/**/     # live or post-processed

        - gps.csv -> time, lat, lon
        - image -> type
        - accelerometer.csv -> quality

    output/000/landmarks.csv: [time, lat, lon, type, quality]

        map matching:
        - lat, lon -> lat*, lon*
        - step_index -> way_id
        segment matching:
        - segment_id
        route matching:
        - time -> date -> route_id
        - time/folder_name -> track_id ("data/raw/000" -> 0)

    output/000/waypoints.csv: [route_id, track_id, way_id, segment_id, type, quality]

        route stats:

    output/000/routes.geojson: [route_id, date, length, type?, quality?]    

        segment stats:

    output/000/segments.geojson: [segment_id, type, quality]

        way stats:

    output/000/ways.geojson: [way_id, type, quality]

        FINALLY
        plotting all .geojson files as separate layers:

    output/000/map.html