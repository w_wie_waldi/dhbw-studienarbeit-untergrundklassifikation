from components import Camera, GPS, Accelerometer, Menu, Item
from road_quality.classifier import RoadQualityClassifier
from road_type.classifier import RoadTypeClassifier
import cv2
from torchvision import transforms
import pandas as pd
import numpy as np
from pathlib import Path
import time, datetime
import threading, subprocess, queue
import shutil
import logging


class System:
    def __init__(self):
        self.roadSurfaceClassifier = RoadSurfaceClassifier()
        self.menu = Menu(
            items=[
                "Back to Dashboard",
                "Start Record",
                "Exit Program",
                #"Shutdown System"
            ]
        )
        logging.basicConfig(format="%(asctime)-15s [%(levelname)s] %(funcName)s in %(filename)s: %(message)s", level=logging.INFO)
        logging.info("System running")

    def run(self):
        """Function to call to run the system."""
        try:
            self.running = True
            self.roadSurfaceClassifier.start()

            while self.running:
                # show dashboard until button switch
                switch = False
                while not switch:
                    switch = self.menu.dashboard(self.roadSurfaceClassifier.info())

                # draw menu and get selected item
                item = self.menu.menu()

                # continue if back button
                if item == 0:
                    continue

                # else show a dialog
                if not self.menu.dialog():
                    continue

                # if confirmed, execute action
                if item == 1:
                    if not self.roadSurfaceClassifier.recording:
                        self.roadSurfaceClassifier.start_record()
                        self.menu.items[1] = "Stop Record"
                    else:
                        self.roadSurfaceClassifier.stop_record()
                        self.menu.items[1] = "Start Record"
                elif item == 2:
                    self.running = False
        
        except:
            logging.exception("")
        finally:
            self.roadSurfaceClassifier.stop()
            self.menu.clear()

    def _shutdown(self):
        """Force the Jetson to shut down."""
        subprocess.run(["shutdown now -h"], shell=True)


class RoadSurfaceClassifier:
    """Data handler for gps module, camera and accelerometer data streams and road type and quality classifier"""

    def __init__(self):
        """Record data to given directory."""
        ### PATHS ###
        self.dir = Path("/studienarbeit/data/raw")

        ### QUEUES ###
        self.main_output = queue.Queue()
        self.type_output = queue.Queue()
        self.quality_output = queue.Queue()
        
        ### HARDWARE ###
        self.gps = GPS()
        self.camera = Camera()
        self.accelerometer = Accelerometer(sample_rate=1000, cam_enable=False)

        ### RECORD ###
        self.recording = False
        self.record_time = np.nan
        
    def start(self):
        """Starting and creating all data input streams."""
        logging.info("Starting RoadSurfaceClassifier")
        self.running = True
        self.main_thread = threading.Thread(target=self._main)
        self.main_thread.start()
        self.type_thread = threading.Thread(target=self._type)
        self.type_thread.start()
        self.quality_thread = threading.Thread(target=self._quality)
        self.quality_thread.start()

    def stop(self):
        """Stop all data streams."""
        logging.info("Stopping RoadSurfaceClassifier")
        self.stop_record()
        self.running = False
        if self.main_thread.is_alive():
            self.main_thread.join()
        if self.type_thread.is_alive():
            self.type_thread.join()
        if self.quality_thread.is_alive():
            self.quality_thread.join()

    def info(self):
        """Return bundled information about current location, road and the recording."""
        # road data
        try:
            info = self.main_output.get(timeout=1).to_dict()
        except queue.Empty:
            info = {
                "time": round(time.time(), 3),
                "latitude": np.nan,
                "longitude": np.nan,
                "altitude": np.nan,
                "road_type": np.nan,
                "road_quality": np.nan
            }
        # record time
        if not np.isnan(self.record_time):
            info["record_time"] = int(time.time() - self.record_time)
        else:
            info["record_time"] = np.nan
        # storage
        total, used, free = shutil.disk_usage("/")
        info["storage"] = used/total
        return info

    def start_record(self):
        """Start a new record."""
        if not self.recording:
            logging.info("Starting new record")
            self.create_record_dir()
            self.recording = True
            self.record_time = time.time()

    def stop_record(self):
        """Stop the record."""
        if self.recording:
            logging.info("Stopping record")
            self.recording = False
            self.record_time = np.nan

    def create_record_dir(self):
        """Determine path names for a new record and create folder."""
        date = datetime.datetime.now()
        number = len([item for item in self.dir.glob("*_*") if item.is_dir()])
        dir = self.dir/"{:0>3}_{}".format(number, date.strftime("%d%m%y"))

        self.paths = {
            "record": dir,
            "camera": dir/"images",
            "accelerometer": dir/"accelerometer.csv",
            "road": dir/"road.csv"
        }
        self.paths["camera"].mkdir(0o777, parents=True)

    def _image_path(self):
        """Create path for a new image."""
        dir = self.paths["camera"]
        utc = round(time.time())
        number = len([image for image in dir.glob("{}_*".format(utc))])
        path = dir/"{}_{}.jpg".format(utc, number)
        return str(path)

    def _main(self):
        """Main thread for bringing together all classification results and link them to a location."""
        try:
            self.gps.on()
            columns = ["time", "latitude", "longitude", "altitude", "road_type", "road_quality"]

            while self.running:
                try:
                    # get location and wrap collected data
                    location = self.gps.read()
                    type = self.type_output.get(timeout=1) if self.type_thread.is_alive() else np.nan
                    quality = self.quality_output.get(timeout=1) if self.quality_thread.is_alive() else np.nan
                    road_data = pd.Series([*location.to_list(), type, quality], index=columns)
                    
                    # empty the queue
                    if not self.main_output.empty():
                        self.main_output.get_nowait()

                    # put road data into the main queue
                    self.main_output.put(road_data)
                
                    # save gps and road data if currently recording
                    if self.recording:
                        new = not self.paths["road"].exists()
                        pd.DataFrame([road_data]).to_csv(self.paths["road"], mode="a", index=False, header=new)
                
                except queue.Empty:
                    pass

        except:
            logging.exception("")
        finally:
            self.gps.off()

    def _type(self):
        """Road type classifier thread using camera stream."""
        try:
            self.camera.on()
            classifier = RoadTypeClassifier()
            converter = transforms.ToTensor()

            while self.running:
                # get camera image
                raw_image = self.camera.read()
                image = converter(cv2.resize(raw_image, (128, 128))).unsqueeze(0)
                type = classifier.classify(image)

                # empty the queue
                if not self.type_output.empty():
                    try:
                        self.type_output.get_nowait()
                    except queue.Empty:
                        pass

                # and put new data into the queue
                self.type_output.put(type)

                # save data if currently recording
                if self.recording:
                    cv2.imwrite(self._image_path(), raw_image)
                
        except:
            logging.exception("")
        finally:
            self.camera.off()

    def _quality(self):
        """Road quality classifier thread using accelerometer stream."""
        try:
            self.accelerometer.on()
            classifier = RoadQualityClassifier()
            intervall_s = 1
            sample_i = []

            while self.running:
                # read sensor data
                sample_t = self.accelerometer.read()
                sample_i.append(sample_t)

                # perform classification if interval is collected
                if sample_i[-1]["time"]-sample_i[0]["time"] >= intervall_s:
                    quality = classifier.classify(pd.DataFrame(sample_i))
                    sample_i = []

                    # empty the queue
                    if not self.quality_output.empty():
                        try:
                            self.quality_output.get_nowait()
                        except queue.Empty:
                            pass

                    # and put new data into the queue
                    self.quality_output.put(quality)

                # save data if currently recording
                if self.recording:
                    new = not self.paths["accelerometer"].exists()
                    pd.DataFrame([sample_t]).to_csv(self.paths["accelerometer"], mode="a", index=False, header=new)
                
        except:
            logging.exception("")
        finally:
            self.accelerometer.off()


def main():
    system = System()
    system.run()

if __name__=="__main__":
    main()