# Studienarbeiten

![DHBW Logo](docs/dhbw.png)


> **5\. Semester: T3_3100**  
> *"Integration eines echtzeitfähigen Convolutional Neural Network zur monokamerabasierten Untergrundklassifikation"*  
> 10/2021 ⎯ 01/2022

> **6\. Semester: T3_3200**  
> *"Integration eines echtzeitfähigen Convolutional Neural Network zur monokamerabasierten Untergrundklassifikation Teil II"*  
> 01/2022 ⎯ 05/2022

des Studiengangs Mechatronik an der Dualen Hochschule Baden-Württemberg Stuttgart

by [Waldemar Dick](mailto:mt19029@lehre.dhbw-stuttgart.de) and [Vaihunthan Vyramuthu](mailto:mt19004@lehre.dhbw-stuttgart.de)

## Setup

### Repository

Clone repository in root:

    cd /
    git clone https://w_wie_waldi@bitbucket.org/w_wie_waldi/studienarbeit.git
    sudo chmod 777 -R studienarbeit

### Python 3.9

Prepare linux apt for Python:

    sudo apt install software-properties-common
    sudo add-apt-repository ppa:deadsnakes/ppa

Download Python:

    sudo apt install python3.9
    sudo apt install python3.9-distutils
    sudo apt-get install python3.9-dev

Install virtual environment module for Python:
    
    sudo apt-get install python3.9-venv
    python3.9 -m pip install --user virtualenv
    python3.9 -m pip install --upgrade pip

### Virtual Environment

Create a new virtual environment (i.e. in ``~/``):

    python3.9 -m venv ~/studienarbeit

Activate the new environment:

    source ~/studienarbeit/bin/activate

Install all necessary packages that are listed in [``requirements.txt``](requirements.txt):

    pip install --upgrade pip
    pip install -r /studienarbeit/requirements.txt

NOTE: if thats not working properly install packages manually.

### GStreamer for OpenCV

By default the PiP build for OpenCV has no GStreamer capability:

    python
    >>> import cv2
    >>> print(cv2.getBuildInformation())

    General configuration for OpenCV 4.5.5 =====================================
    ⋮
    Video I/O:
        ⋮
        GStreamer:                   NO    <--- disabled

Therefore OpenCV needs to be rebuild with Cmake as described [here](https://galaktyk.medium.com/how-to-build-opencv-with-gstreamer-b11668fa09c).

Install GStreamer packages:

    sudo apt-get install gstreamer1.0*
    sudo apt install ubuntu-restricted-extras
    sudo apt install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
    sudo apt install gstreamer1.0-python3-plugin-loader

Download OpenCV from source to ``~/``:

    cd ~/
    git clone https://github.com/opencv/opencv.git

Install numpy to environment if not already installed by [``requirements.txt``](requirements.txt):

    source ~/studienarbeit/bin/activate
    pip install numpy

Configure Cmake build:

    mkdir opencv/build/
    cd opencv/build/

    cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D PYTHON_EXECUTABLE=$(which python3) \
    -D BUILD_opencv_python2=OFF \
    -D CMAKE_INSTALL_PREFIX=$(python3 -c "import sys; print(sys.prefix)") \
    -D PYTHON3_EXECUTABLE=$(which python3) \
    -D PYTHON3_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
    -D PYTHON3_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
    -D WITH_GSTREAMER=ON \
    -D BUILD_EXAMPLES=ON ..

Then check build configuration:

    ⋮
    Video I/O:
        ⋮
        GStreamer:                   YES (1.14.5)    <--- enabled
    ⋮
    Python 3:
        Interpreter:                 ~/studienarbeit/bin/python3 (ver 3.9.10)    <--- virtual environment python
        Libraries:                   /usr/lib/aarch64-linux-gnu/libpython3.9.so (ver 3.9.10)
        numpy:                       ~/studienarbeit/lib/python3.9/site-packages/numpy/core/include (ver 1.22.1)
        install path:                ~/studienarbeit/lib/python3.9/site-packages/cv2/python-3.9
    ⋮
    Install to:                      ~/studienarbeit    <--- virtual environment

Build and finish OpenCV installation:

    sudo make -j$(nproc)
    sudo make install
    sudo ldconfig

Validate installation:

    python -c "import cv2; print(cv2.getBuildInformation())"

    General configuration for OpenCV 4.5.5 =====================================
    ⋮
    Video I/O:
        ⋮
        GStreamer:                   YES (1.14.5)    <--- installation successful

### [IMX219 camera: fix red tint](https://www.arducam.com/docs/camera-for-jetson-nano/fix-red-tint-with-isp-tuning/)

Download and unzip camera correction files:

    wget https://www.arducam.com/downloads/Jetson/Camera_overrides.tar.gz
    tar zxvf Camera_overrides.tar.gz

Replace default camera files:

    sudo cp camera_overrides.isp /var/nvidia/nvcam/settings/
    sudo chmod 664 /var/nvidia/nvcam/settings/camera_overrides.isp
    sudo chown root:root /var/nvidia/nvcam/settings/camera_overrides.isp

Restart system:

    sudo reboot

### Serial Communication

For the first time you have to give permission to read the serial port of the gps module:

    sudo usermod -a -G dialout $USER

    systemctl stop nvgetty
    systemctl disable nvgetty
    sudo reboot

### Enable autostart

To enable autostart of the python script, create a new service inside ``/etc/systemd/system/``:

    sudo nano /etc/systemd/system/autostart.service

With following contents:

    [Unit]
    Description=Run autostart.sh of the studienarbeit repository on boot

    [Service]
    ExecStart=/studienarbeit/autostart.sh

    [Install]
    WantedBy=multi-user.target

Then activate the service and check status:

    sudo systemctl start autostart
    sudo systemctl status autostart
    sudo systemctl enable autostart


## Repository structure

    .
    ├── data/                               # data saved in OneDrive
    ├── docs/                               # images
    ├── components/
    │   ├── accelerometer/
    │   ├── camera/
    │   ├── display/
    │   └── gps/
    ├── road_type/
    │   ├── models/                         # cnn models
    │   ├── classification.ipynb            # classification test
    │   ├── training.ipynb                  # training script
    │   └── model.py                        # main cnn models
    ├── utils/
    ├── main.py                             # main program
    ├── raw.py*                             # record raw sensor data
    ├── requirements.txt
    ├── PROCESSES.md                        # desciption of data processing
    └── README.md


## data/

    ├── data/
    │   ├── datasets/                       # datasets
    │   │   ├── 000/
    │   │   │   ├── images/
    │   │   │   │   ├── asphalted/
    │   │   │   │   ├── paved/
    │   │   │   │   └── unpaved/
    │   │   │   ├── acceleration*/
    │   │   │   │   ├── good.csv*
    │   │   │   │   ├── medium.csv*
    │   │   │   │   └── bad.csv*
    │   │   │   ├── gps.csv*
    │   │   │   └── config.json
    │   │   :
    │   ├── models/                         # neural networks
    │   │   ├── resnet34_d000_e000.pth
    │   │   :
    │   ├── output/                      # processed and classified data and maps
    │   │   ├── 000/                            # resnet34 qual_x_f8-16_t16-32
    │   │   │   ├── landmarks.csv (000-056)
    │   │   │   ├── waypoints.csv
    │   │   │   ├── (track.gpx)
    │   │   │   ├── map.html
    │   │   │   ├── routes.geojson
    │   │   │   ├── segments.geojson
    │   │   │   ├── ways.geojson
    │   │   │   └── config.json
    │   │   ├── 001/                            # resnet34 qual_x_f8-16_t16-32
    │   │   │   ├── landmarks.csv (000-056) + (057-067)
    │   │   │   ├── waypoints.csv
    │   │   │   ├── (track.gpx)
    │   │   │   ├── map.html
    │   │   │   ├── routes.geojson
    │   │   │   ├── segments.geojson
    │   │   │   ├── ways.geojson
    │   │   │   └── config.json
    │   │   ├── 002/                            # resnet18 qual_x_f8-16_t16-32
    │   │   │   ├── landmarks.csv (068-075) + (000-067)
    │   │   │   ├── waypoints.csv
    │   │   │   ├── (track.gpx)
    │   │   │   ├── map.html
    │   │   │   ├── routes.geojson
    │   │   │   ├── segments.geojson
    │   │   │   ├── ways.geojson
    │   │   │   └── config.json
    │   │   :
    │   ├── raw/                            # raw data
    │   │   ├── 000/
    │   │   │   ├── images/
    │   │   │   ├── accelerometer.csv
    │   │   │   ├── accelerometer_0.csv*
    │   │   │   ├── gps.csv
    │   │   │   └── gps_0.csv*
    │   │   :
    │   ├── segments/                       # segments for segment matching
    │   │   ├── 10m.csv
    │   │   :
    :   :


### raw/000.json

    {
        "track": "000",
        "date": "22.02.2022",
        "start_time": 1645525908,
        "end_time": 1645526815,
        "record_duration_s": 907,
        "images": 9071,
        "acceleration": 271104,
        "labels": {
            "road_type": [
                [
                    0,
                    1645525908,
                    1645526021
                ],
                [
                    1,
                    1645526022,
                    1645526023
                ],
                :
            ],
            "road_quality": [
                [
                    0,
                    1645525908,
                    1645526023
                ],
                [
                    1,
                    1645526024,
                    1645526045
                ],
                :
            ]
        }
    }