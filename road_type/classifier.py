from .model import RoadSurfaceClassificator, _resnet18, _resnet34
import torch


class RoadTypeClassifier:
    def __init__(self):
        ### MODEL ###
        if True:
            self.model = _resnet18()
            model_path = "/studienarbeit/data/models/resnet18_dataset000_epoch1.pth"
        else:
            self.model = _resnet34()
            model_path = "/studienarbeit/data/models/resnet34_1_dataset001_epoch1.pth"
        self.model.load_state_dict(torch.load(model_path))
    
    def classify(self, image):
        """Perform classification of road type on single image."""
        outputs = self.model(image)

        _, type_index = torch.max(outputs, 1)
        probability = torch.softmax(outputs, 1)[0, type_index]

        return int(type_index), round(float(probability), 3)