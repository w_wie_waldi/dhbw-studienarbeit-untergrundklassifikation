import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models import resnet18, resnet34

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class ImageClassificationBase(nn.Module):
    
    def training_step(self, batch):
        images, labels = batch 
        out = self(images)                  # Generate predictions
        loss = F.cross_entropy(out, labels) # Calculate loss
        return loss
    
    def validation_step(self, batch):
        images, labels = batch 
        out = self(images)                    # Generate predictions
        loss = F.cross_entropy(out, labels)   # Calculate loss
        acc = accuracy(out, labels)           # Calculate accuracy
        print(out)
        print(labels)
        raise Exception
        return {'val_loss': loss.detach(), 'val_acc': acc}
        
    def validation_epoch_end(self, outputs):
        batch_losses = [x['val_loss'] for x in outputs]
        epoch_loss = torch.stack(batch_losses).mean()   # Combine losses
        batch_accs = [x['val_acc'] for x in outputs]
        epoch_acc = torch.stack(batch_accs).mean()      # Combine accuracies
        return {'val_loss': epoch_loss.item(), 'val_acc': epoch_acc.item()}
    
    def epoch_end(self, epoch, result):
        # #####
        # torch.save(self.state_dict(), self.model_dir / ("road_type_" + epoch + ".pth"))
        # #####
        print("Epoch [{}], train_loss: {:.4f}, val_loss: {:.4f}, val_acc: {:.4f}".format(
            epoch, result['train_loss'], result['val_loss'], result['val_acc']))


class RoadSurfaceClassificatorLight(ImageClassificationBase):
    def __init__(self):
        super().__init__()
        #self.model_dir = model_dir
        self.network = nn.Sequential(
            ## 128×128×3 ##
            nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            ## 64×64×32 ##
            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            ## 32×32×64 ##
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            ## 16×16×128 ##
            nn.Flatten(),
            nn.Linear(32768, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 3)
        )
    
    def forward(self, xb):
        return self.network(xb)


class _resnet18(ImageClassificationBase):
    def __init__(self):
        super().__init__()
        self.network = nn.Sequential(
            resnet18(pretrained=True), 
            nn.Linear(1000, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 3)
        )
    
    def forward(self, xb):
        return self.network(xb)

class _resnet34(ImageClassificationBase):
    def __init__(self):
        super().__init__()
        self.network = nn.Sequential(
            resnet34(pretrained=True),
            nn.Linear(1000, 1024),

            #nn.Linear(1000, 512),
            # 128×128×3 ##
            # nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.MaxPool2d(2, 2),
            # ## 64×64×64 ##
            # nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.MaxPool2d(2, 2),
            # ## 32×32×128 ##
            # nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.MaxPool2d(2, 2),
            # ## 16×16×256 ##
            # nn.Flatten(),
            # nn.Linear(65536, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 3)
        )
    
    def forward(self, xb):
        return self.network(xb)

class RoadSurfaceClassificator(ImageClassificationBase):
    def __init__(self, model_dir:str):
        super().__init__()
        self.model_dir = model_dir
        self.network = nn.Sequential(
            resnet34(pretrained=True), 
            nn.Linear(1000, 1024),

            #nn.Linear(1000, 512),
            # 128×128×3 ##
            # nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.MaxPool2d(2, 2),
            # ## 64×64×64 ##
            # nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.MaxPool2d(2, 2),
            # ## 32×32×128 ##
            # nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            # nn.MaxPool2d(2, 2),
            # ## 16×16×256 ##
            # nn.Flatten(),
            # nn.Linear(65536, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            # nn.ReLU(),
            nn.Linear(512, 3)
        )
    
    def forward(self, xb):
        return self.network(xb)


def accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))

  
@torch.no_grad()
def evaluate(model, val_loader):
    model.eval()
    #outputs = [model.validation_step(batch) for batch in val_loader]
    outputs = []
    for batch in val_loader:
        inputs, labels = batch
        inputs, labels = inputs.to(device), labels.to(device)
        outputs.append(model.validation_step((inputs, labels)))
    return model.validation_epoch_end(outputs)

  
def fit(epochs, lr, model, train_loader, val_loader, opt_func=torch.optim.SGD):
    
    history = []
    optimizer = opt_func(model.parameters(),lr)
    for epoch in range(epochs):
        
        model.train()
        train_losses = []
        for batch in train_loader:

            inputs, labels = batch
            inputs, labels = inputs.to(device), labels.to(device)
            
            loss = model.training_step((inputs, labels))
            train_losses.append(loss)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

            torch.cuda.empty_cache()
            
        result = evaluate(model, val_loader)
        result['train_loss'] = torch.stack(train_losses).mean().item()
        model.epoch_end(epoch, result)
        history.append(result)

        torch.cuda.empty_cache()
    
    return history