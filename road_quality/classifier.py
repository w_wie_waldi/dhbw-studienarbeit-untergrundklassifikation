import numpy as np
from scipy.fft import rfft, rfftfreq


class RoadQualityClassifier:
    def __init__(self):
        ### PARAMETERS ###
        self.threshold_01 = 16
        self.threshold_12 = 32

    def _classify(self, sample):
        ffts = self.get_fft(sample, axes=["z"], range=[8, 16])
        means = np.mean(ffts, axis=2)
        indicator = np.sum(means)
        return self.get_quality(indicator)

    def classify(self, sample):
        ffts = self.get_fft(sample)
        stats = self.get_stats(ffts)
        indicator = self.get_indicator(stats)
        quality = self.get_quality(indicator)
        return quality
    
    def get_fft(self, sample, accelerometers=["box", "cam"], axes=["x", "z"], range=[1, 20]):
        #t_sample = sample["time"][-1] - sample["time"][0]
        #n_sample = sample.shape[0]

        ffts_ac = []

        # box, cam
        for accelerometer in accelerometers:
            ffts_ax = []

            # x, z
            for axis in axes:
                # e.g. sample["x_box"]
                values = sample[axis+"_"+accelerometer].to_numpy()
                
                # get frequency bins and perform fft
                # assuming t_sample is constant with 1s
                # we get bins from [0, 1, ... int(n_sample/2)]
                # y_fft are corresponding frequency values
                y_fft = np.abs(rfft(values))

                # crop fft values from f_range
                y_fft = y_fft[range[0]:range[1]+1]
                ffts_ax.append(y_fft)
            
            ffts_ac.append(ffts_ax)
        
        return np.array(ffts_ac)

    def get_stats(self, ffts):
        """
        ffts: array with two rows for box and cam containing fft values for x and z axis
            np.array([[x_box, z_box],
                      [x_cam, z_cam]])
        """
        
        means = np.mean(ffts, axis=2)
        #maxes = np.max(ffts, axis=2)

        #means = np.sum(ffts, axis=2)
        maxes = np.sum(np.abs(np.diff(ffts)), axis=2)

        #means = np.mean(np.sort(ffts)[:,-25:], axis=2)
        return np.concatenate((means, maxes), axis=1)
    
    def get_indicator(self, stats, accelerometers=[1, 1], axes=[1, 2], mean_max=[2, 1]):
        """
        stats: array with two rows for box and cam containing x_mean, x_max, z_mean, z_max
            np.array([[x_box_mean, z_box_mean, x_box_max, z_box_max],
                      [x_cam_mean, z_cam_mean, x_cam_max, z_cam_max]])

        accelerometers: weigth of box over cam
            [box, cam]
        
        axes: weigth of x over z axis
            [x_axis, z_axis]
        
        mean_max: weigth of mean over max
            [mean, max]
        """
        
        _accelerometers = np.c_[accelerometers]
        _axes = np.array([axes])
        _mean_max = np.c_[mean_max]

        return int(np.array([stats @ (_mean_max @ _axes).flatten()]) @ _accelerometers)

    def get_quality(self, indicator):
        if indicator < self.threshold_01:
            return 0
        elif indicator < self.threshold_12:
            return 1
        else:
            return 2