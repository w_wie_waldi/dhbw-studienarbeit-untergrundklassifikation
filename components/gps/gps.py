import serial, pynmea2
import time, logging, threading, queue
import pandas as pd


class GPS():
    """GPS module class with bufferless output stream."""

    def __init__(self):
        self.running = False
        self.output = queue.Queue()

    def _configure(self):
        """Apply configurations, disable unused GPS sentences."""
        configs = [
            b"$PUBX,40,RMC,0,0,0,0,0,0*47\r\n", # disable GPRMC
            b"$PUBX,40,GSA,0,0,0,0,0,0*4E\r\n", # disable GPGSA
            b"$PUBX,40,GSV,0,0,0,0,0,0*59\r\n", # disable GPGSV
            b"$PUBX,40,GLL,0,0,0,0,0,0*5C\r\n", # disable GPGLL
            b"$PUBX,40,VTG,0,0,0,0,0,0*5E\r\n", # disable GPVTG
            b"\xB5\x62\x06\x08\x06\x00\x64\x00\x01\x00\x01\x00\x7A\x12", # 10Hz
        ]
        for config in configs:
            self.input.write(config)

    def _reader(self):
        """Read data as soon as it is available, keeping only most recent."""
        columns = ["time", "latitude", "longitude", "altitude"]
        while self.running:
            try:
                sentence = self.input.readline().decode()
                data = pynmea2.parse(sentence)
                gps_data = [
                    round(time.time(), 3),
                    self._convert(data.lat),
                    self._convert(data.lon),
                    data.altitude
                ]
            except (UnicodeDecodeError, pynmea2.nmea.ParseError, AttributeError):
                gps_data = [
                    round(time.time(), 1),
                    None,
                    None,
                    None
                ]

            # empty the queue
            if not self.output.empty():
                try:
                    self.output.get_nowait()
                except queue.Empty:
                    pass

            # put gps data into output queue
            self.output.put(pd.Series(gps_data, index=columns))
            
    def _convert(self, string):
        if string == "":
            return None
        try:
            degree = int(string[:-8])
            minute = float(string[-8:])
            return round(degree + minute/60, 8)
        except Exception as e:
            logging.error(e)
            return None

    def on(self):
        """Open the serial port and start the reader thread."""
        self.input = serial.Serial(port="/dev/ttyTHS1", baudrate=9600)
        self._configure()
        self.running = True
        self.thread = threading.Thread(target=self._reader)
        self.thread.start()

    def read(self):
        """Read latest data from output stream."""
        try:
            return self.output.get(timeout=3)
        except queue.Empty:
            self.running = False
            raise queue.Empty

    def off(self):
        """Stop the reader thread and close the serial port."""
        if self.running:
            self.running = False
            self.thread.join()
            self.input.close()