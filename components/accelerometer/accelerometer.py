from mpu9250_jmdev.registers import *
from mpu9250_jmdev.mpu_9250 import MPU9250
import time, threading, queue
import pandas as pd
import numpy as np


class Accelerometer():
    """Accelerometer class for both accelerometers with output stream."""

    def __init__(self, sample_rate, box_enable=True, cam_enable=True):
        self.running = False
        self.output = queue.Queue()
        self.sample_rate = sample_rate
        self.box_enable = box_enable
        self.cam_enable = cam_enable
        self.decimals = 12
        
    def _configure(self):
        """Apply configurations."""
        if self.box_enable:
            self.input_box.configure()
        if self.cam_enable:
            self.input_cam.configure()
        if False:
            # some configurations
            self._i2c_write(SMPLRT_DIV, 0x00)
            self._i2c_write(CONFIG, 0x00)
            self._i2c_write(GYRO_CONFIG, 0x03)
            self._i2c_write(ACCEL_CONFIG_2, 0x08)

    def _reader(self):
        """Read data as soon as it is available, keeping only most recent data."""
        columns = ["time", "x_box", "y_box", "z_box", "x_cam", "y_cam", "z_cam"]
        while self.running:
            t_0 = time.time()
            t = t_0 + (1/self.sample_rate)

            if self.box_enable:
                box_0 = self.input_box.readAccelerometerMaster()
                box = self.box_to_bicycle(box_0)
            else:
                box = np.c_[[None, None, None]]

            if self.cam_enable:
                cam_0 = self.input_cam.readAccelerometerMaster()
                cam = self.cam_to_bicycle(cam_0)
            else:
                cam = np.c_[[None, None, None]]

            if not self.output.empty():
                try:
                    self.output.get_nowait() # empty the queue
                except queue.Empty:
                    pass

            self.output.put(pd.Series([round(t_0, 3), *box.flat, *cam.flat], index=columns))
            time.sleep(max(0, t-time.time()))

    def _i2c_write(self, register, data, sleep=0):
        self.input_cam.bus.write_byte_data(self.input_cam.address_mpu_master, register, data)
        if sleep > 0:
            time.sleep(sleep)

    def _i2c_read(self, register, quantity):
        return self.input_cam.bus.read_i2c_block_data(self.input_cam.address_mpu_master, register, quantity)

    def on(self):
        """Connect to the accelerometers and start the reader thread."""
        if self.box_enable:
            self.input_box = MPU9250(
                address_ak=AK8963_ADDRESS,
                address_mpu_master=MPU9050_ADDRESS_69,
                address_mpu_slave=None,
                bus=1,
                gfs=GFS_1000,
                afs=AFS_8G,
                mfs=AK8963_BIT_16,
                mode=AK8963_MODE_C100HZ
            )
        if self.cam_enable:
            self.input_cam = MPU9250(
                address_ak=AK8963_ADDRESS,
                address_mpu_master=MPU9050_ADDRESS_68,
                address_mpu_slave=None,
                bus=1,
                gfs=GFS_1000,
                afs=AFS_8G,
                mfs=AK8963_BIT_16,
                mode=AK8963_MODE_C100HZ
            )
        self._configure()
        self.running = True
        self.thread = threading.Thread(target=self._reader)
        self.thread.start()

    def read(self):
        """Read latest data from output stream."""
        try:
            return self.output.get(timeout=3)
        except queue.Empty:
            self.running = False
            raise queue.Empty

    def off(self):
        """Stop the reader thread."""
        if self.running:
            self.running = False
            self.thread.join()
    
    def box_to_bicycle(self, vector):
        """Apply -145° x axis rotation and 90° z axis rotation."""
        return np.round((
                np.array([
                    [ 6.12323400e-17,  8.19152044e-01, -5.73576436e-01],
                    [ 1.00000000e+00, -5.01585965e-17,  3.51214273e-17],
                    [ 0.00000000e+00, -5.73576436e-01, -8.19152044e-01]
                ]) @ vector
            ), decimals=self.decimals)

    def cam_to_bicycle(self, vector):
        """Apply 43° x axis rotation and -90° z axis rotation."""
        return np.round((
                np.array([
                    [ 6.12323400e-17,  7.31353702e-01, -6.81998360e-01],
                    [-1.00000000e+00,  4.47824985e-17, -4.17603554e-17],
                    [ 0.00000000e+00,  6.81998360e-01,  7.31353702e-01]
                ]) @ vector
            ), decimals=self.decimals)