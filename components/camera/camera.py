import cv2
import threading, queue

 
class Camera:
    """Camera class with bufferless output stream."""

    def __init__(self):
        self.running = False
        self.output = queue.Queue()
        
    def _gstreamer_pipeline(self, raw_width=1640, raw_height=1232, out_width=340, out_height=256, framerate=10, flip_method=2):
        """Command to initialize and access the camera pipeline."""
        return (
            "nvarguscamerasrc"
            " ! video/x-raw(memory:NVMM), width=(int){:d}, height=(int){:d}, format=(string)NV12, framerate=(fraction){:d}/1"
            " ! nvvidconv flip-method={:d}"
            " ! video/x-raw, width=(int){:d}, height=(int){:d}, format=(string)BGRx"
            " ! videoconvert"
            " ! video/x-raw, format=(string)BGR"
            " ! appsink"
            .format(
                raw_width,
                raw_height,
                framerate,
                flip_method,
                out_width,
                out_height,
            )
        )

    def _reader(self):
        """Read frames as soon as they are available, keeping only most recent one."""
        while self.running:
            ret, frame = self.input.read()
            if not ret:
                break
            if not self.output.empty():
                try:
                    self.output.get_nowait() # discard previous frame
                except queue.Empty:
                    pass
            self.output.put(frame)

    def on(self):
        """Access the camera ressource and start the reader thread."""
        self.input = cv2.VideoCapture(self._gstreamer_pipeline(), cv2.CAP_GSTREAMER)
        self.thread = threading.Thread(target=self._reader)
        self.running = True
        self.thread.start()

    def read(self):
        """Read latest frame from camera output stream."""
        try:
            return self.output.get(timeout=3)
        except queue.Empty:
            self.running = False
            raise queue.Empty

    def off(self):
        """Stop the reader thread and release the camera ressource."""
        if self.running:
            self.running = False
            self.thread.join()
            self.input.release()