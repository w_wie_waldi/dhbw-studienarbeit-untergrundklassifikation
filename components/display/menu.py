import time, datetime
from rpi_lcd import LCD
from .encoder import Encoder
import numpy as np


class Menu:

    def __init__(self, items):
        ### MENU ###
        self.items = items
        self._position = 0
        self.pointer = "\xA5"
        self._switch = False
        self._dialog = False
        self._confirm = False
        self.indicator = "\x2E\xA5"
        self.indicator_i = 0
        self.running = False
        self.road_type = ["asphalted", "paved", "unpaved"]
        self.road_quality = ["good", "medium", "bad"]

        ### DISPLAY ###
        self.lcd = LCD(bus=1, width=20, rows=4)
        self.REFRESH_TIME_S = 0.5

        ### ROTARY ENCODER ###
        self.encoder = Encoder(26, 24, 29, self.on_left, self.on_right, self.on_switch)

    def on_left(self):
        """Callback function for the encoder turning left."""
        if not self._dialog:
            self._position = (self._position - 1) % len(self.items)
        else:
            self._confirm = not self._confirm

    def on_right(self):
        """Callback function for the encoder turning right."""
        if not self._dialog:
            self._position = (self._position + 1) % len(self.items)
        else:
            self._confirm = not self._confirm

    def on_switch(self):
        """Callback function for the encoders switch button."""
        self._switch = True

    def clear(self):
        """Clear the display content."""
        self.lcd.clear()

    def dashboard(self, info):
        """Show the main dashboard once with given information and return whether switch was pushed."""
        t = time.time()+self.REFRESH_TIME_S
        self.draw_dashboard(info)
        time.sleep(max(0, t-time.time()))

        return self._switch

    def draw_dashboard(self, info):
        """Dashboard drawing function."""
        # draw time
        altitude = "{:.0f}m".format(info["altitude"]) if not np.isnan(info["altitude"]) else "-"
        time = "{name1} {time} {name2} {altitude}".format(
            name1="TIM",
            time=datetime.datetime.now().strftime("%H:%M"),
            name2="ALT",
            altitude=altitude
        )
        self.lcd.text(time, 1)

        # draw location
        if np.isnan(info["latitude"]):
            latitude = "-"
        else:
            degree = int(info["latitude"])
            minutes = round((info["latitude"] - degree)*60)
            hemisphere = "N" if degree > 0 else "S"
            latitude = "{}{}\xDF{}\x27".format(hemisphere, degree, minutes)
        if np.isnan(info["longitude"]):
            longitude = "-"
        else:
            degree = int(info["longitude"])
            minutes = round((info["longitude"] - degree)*60)
            hemisphere = "E" if degree > 0 else "W"
            longitude = "{}{}\xDF{}\x27".format(hemisphere, degree, minutes)
        location = "{name} {latitude} {longitude}".format(
            name="LOC",
            latitude=latitude,
            longitude=longitude
        )
        self.lcd.text(location, 2)
        
        # draw road
        if np.isnan(info["road_type"]):
            type = "-"
        else:
            type = self.road_type[int(info["road_type"])]
        if np.isnan(info["road_quality"]):
            quality = "-"
        else:
            quality = self.road_quality[int(info["road_quality"])]
        road = "{name} {type} {quality}".format(
            name="SUR",
            type=type,
            quality=quality
        )
        self.lcd.text(road, 3)

        # draw record
        if np.isnan(info["record_time"]):
            record_time = "--:--"
        else:
            minutes = int(info["record_time"]/60)
            hours = int(minutes/60)
            if not hours:
                seconds = info["record_time"] - minutes*60
                record_time = "{:0>2}:{:0>2}".format(minutes, seconds)
            else:
                minutes = minutes - hours*60
                record_time = "{:0>2}:{:0>2}".format(hours, minutes)
        record = "{name1} {record_time} {name2} {storage:.1%}".format(
            name1="REC",
            record_time=record_time,
            name2="STO",
            storage=info["storage"]
        )
        self.lcd.text(record, 4)

    def menu(self):
        """Show the menu consisting of initially given menu items. Handles the encoder inputs on its own. Returns the selected menu item as an integer if switch was pressed."""
        self._switch = False
        self._position = 0

        self.lcd.clear()
        while not self._switch:
            t = time.time()+self.REFRESH_TIME_S
            self.draw_menu()
            time.sleep(max(0, t-time.time()))
        self._switch = False

        return self._position

    def draw_menu(self):
        """Menu drawing function."""
        # draw heading
        heading = "{title:19}{indicator}".format(
            title="MENU",
            indicator=self.indicator[self.indicator_i]
        )
        self.lcd.text(heading, 1)

        # draw items
        for line in range(min(3, len(self.items))):
            item = "{pointer}{name}".format(
                pointer=self.pointer if self._position == line else " ",
                name=self.items[line]
            )
            self.lcd.text(item, line+2)
        
        # update indicator
        self.indicator_i = (self.indicator_i + 1) % len(self.indicator)

    def dialog(self):
        """Show a dialog for confirming the action. Returns true or false."""
        # init dialog
        self._switch = False
        self._dialog = True
        self._confirm = False
        item = self.items[self._position]
        
        # draw screen and wait for answer
        self.lcd.clear()
        while not self._switch:
            t = time.time()+self.REFRESH_TIME_S
            self.draw_dialog(item)
            time.sleep(max(0, t-time.time()))
        self._switch = False

        # de-init dialog
        self._dialog = False
        return self._confirm

    def draw_dialog(self, title):
        """Dialog drawing function."""
        # draw heading
        heading = "{title:19}{indicator}".format(
            title=title.upper()+"?",
            indicator=self.indicator[self.indicator_i]
        )
        self.lcd.text(heading, 1)
        
        # draw choices
        no = "{pointer}{name}".format(
            pointer=self.pointer if not self._confirm else " ",
            name="No"
        )
        self.lcd.text(no, 2)
        yes = "{pointer}{name}".format(
            pointer=self.pointer if self._confirm else " ",
            name="Yes"
        )
        self.lcd.text(yes, 3)

        # update indicator
        self.indicator_i = (self.indicator_i + 1) % len(self.indicator)