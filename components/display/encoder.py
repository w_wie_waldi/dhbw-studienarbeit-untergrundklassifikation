import Jetson.GPIO as GPIO


class Encoder:
    """Rotary Encoder with two pins for direction and one for switch. All pins are meant to be active low and execute callback functions on change."""

    def __init__(self, PIN_L, PIN_R, PIN_SW, on_left=None, on_right=None, on_switch=None):
        ### PINS ###
        self.PIN_L = PIN_L
        self.PIN_R = PIN_R
        self.PIN_SW = PIN_SW

        ### CALLBACKS ###
        self.on_left = on_left
        self.on_right = on_right
        self.on_switch = on_switch

        ### GPIO ###
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)
        GPIO.setup([self.PIN_L, self.PIN_R, self.PIN_SW], GPIO.IN)
        GPIO.add_event_detect(self.PIN_L, GPIO.BOTH, callback=self._on_rotation)
        GPIO.add_event_detect(self.PIN_R, GPIO.BOTH, callback=self._on_rotation)
        GPIO.add_event_detect(self.PIN_SW, GPIO.FALLING, callback=self._on_switch, bouncetime=100)

        ### STATES ###
        self.state = "{}{}".format(GPIO.input(self.PIN_L), GPIO.input(self.PIN_R))
        self.direction = None

    def _on_rotation(self, channel):
        """Determine the rotation direction with the help of a state machine.
        
        left turn: 11 -> 10 -> 00 -> 01 -> 11
        right turn: 11 -> 01 -> 00 -> 10 -> 11
        """

        pin_l = GPIO.input(self.PIN_L)
        pin_r = GPIO.input(self.PIN_R)
        newState = "{}{}".format(pin_l, pin_r)
        
        if self.state == "11": # Resting position
            if newState == "01": # Turned right 1
                self.direction = "R"
            elif newState == "10": # Turned left 1
                self.direction = "L"

        elif self.state == "01": # R1 or L3 position
            if newState == "00": # Turned right 1
                self.direction = "R"
            elif newState == "11": # Turned left 1
                if self.direction == "L":
                    if self.on_left is not None:
                        self.on_left()

        elif self.state == "10": # R3 or L1
            if newState == "00": # Turned left 1
                self.direction = "L"
            elif newState == "11": # Turned right 1
                if self.direction == "R":
                    if self.on_right is not None:
                        self.on_right()

        else: # self.state == "00"
            if newState == "01": # Turned left 1
                self.direction = "L"
            elif newState == "10": # Turned right 1
                self.direction = "R"
            elif newState == "11": # Skipped an intermediate 10 or 01 state, but if we know direction then a turn is complete
                if self.direction == "L":
                    if self.on_left is not None:
                        self.on_left()
                elif self.direction == "R":
                    if self.on_right is not None:
                        self.on_right()
        
        self.state = newState

    def _on_switch(self, channel):
        """Calling given function on a switch press."""
        if GPIO.input(self.PIN_SW) == 0: # Skip bounces
            if self.on_switch is not None:
                self.on_switch()

    def __del__(self):
        GPIO.cleanup()
