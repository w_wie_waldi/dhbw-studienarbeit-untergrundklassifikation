import math
from pathlib import Path
import pandas as pd
pd.set_option("mode.chained_assignment", None)
import cv2
from torchvision import transforms
from road_quality.classifier import RoadQualityClassifier
from road_type.classifier import RoadTypeClassifier

import requests
import json
from datetime import datetime
import haversine as hs
import numpy as np
import folium
import logging

from IPython.display import display
import pandas as pd
from pathlib import Path
import requests
import math
import haversine as hs
import numpy as np
import json
from datetime import datetime


class Processor:

    def __init__(self, output_dir=None):
        self.api_url = "https://api.geoapify.com/v1/mapmatching"
        self.api_key = "5632e390f8e54c1fa9bf0cf894d0a4ce"

        ### CONFIG ###
        self.config = {}
        self.config["map_matching_mode"] = "walk"
        self.config["max_segment_length_m"] = 10
        self.config["segment_match_tolerance_m"] = 0.03

        self.raw = Path("data/raw/")
        if output_dir is None:
            self.out = self._output_dir()
            self.out.mkdir(parents=True)
        else:
            self.out = output_dir
        self.segments = Path("/studienarbeit/data/segments/{}m.csv".format(self.config["max_segment_length_m"]))

        logging.basicConfig(format="%(asctime)-15s [%(levelname)s] %(funcName)s in %(filename)s: %(message)s", level=logging.INFO)
        logging.info("Started Classifier.")

    def export_config(self):
        config_path = self.out/"config.json"
        if not config_path.exists():
            with open(config_path, "w") as f:
                json.dump(self.config, f, indent=4)

    def _output_dir(self):
        """Determine the path of the output folder."""
        number = len([f for f in Path("data/output/").glob("*") if f.is_dir()])
        dir = Path("data/output/")/"{:0>3}".format(number)
        return dir

    def create_landmarks(self):
        """
        input: raw/**/

        - gps.csv -> time, lat, lon
        - image -> type
        - accelerometer.csv -> quality

        output: output/000/landmarks.csv: [time, lat, lon, type, quality]
        """

        interval_s = 1

        quality_c = RoadQualityClassifier()
        type_c = RoadTypeClassifier()
        converter = transforms.ToTensor()

        for track in sorted([f for f in self.raw.glob("*") if f.is_dir()]):
            landmarks = []
            gps_data = pd.read_csv(track/"gps.csv").dropna()
            if gps_data.empty:
                continue
            accelerometer_data = pd.read_csv(track/"accelerometer.csv")
            
            # iterate through all coordinates
            logging.info("Creating landmarks for {}.".format(track.name))
            for _, row in gps_data.iterrows():
                t = row["time"]

                # get surface quality
                interval = accelerometer_data[((t - accelerometer_data["time"]) <= interval_s) & ((t >= accelerometer_data["time"]))]
                if interval.shape[0] >= 30:
                    surface_quality = quality_c._classify(interval)
                else:
                    surface_quality = None
                
                # get surface type
                image_path = track/"images/{}_0.jpg".format(int(t))
                if image_path.exists():
                    raw_image = cv2.imread(str(image_path))
                    image = converter(cv2.resize(raw_image, (128, 128))).unsqueeze(0)
                    surface_type, probability = type_c.classify(image)
                else:
                    surface_type = None

                landmarks.append(pd.Series([*row[:-1], surface_type, surface_quality]))
        
            # convert list to dataframe
            new = not (self.out/"landmarks.csv").exists()
            landmarks = pd.DataFrame(landmarks)
            landmarks.columns=[*gps_data.columns[:-1], "type", "quality"]
            landmarks.to_csv(self.out/"landmarks.csv", mode="a", index=False, header=new)

    def create_waypoints(self):
        """
        input: output/000/landmarks.csv: [time, lat, lon, type, quality]

        map matching:
        - lat, lon -> lat*, lon*
        - step_index -> way_id

        segment matching:
        - lat*, lon* -> segment_id

        route matching:
        - time -> date -> route_id
        - time/folder_name -> track_id ("data/raw/000" -> 0)

        output: output/000/waypoints.csv: [route_id, track_id, way_id, segment_id, type, quality]
        """

        landmarks = pd.read_csv(self.out/"landmarks.csv")
        waypoints = self._map_matching(landmarks)
        self._add_segments(waypoints["way_id"])
        waypoints = self._segment_matching(waypoints)
        waypoints = self._route_and_track_matching(waypoints)
        waypoints.to_csv(self.out/"waypoints.csv", index=False)

    def _map_matching(self, landmarks):
        logging.info("Matching landmarks to map.")
        
        def json_waypoints(_landmarks):
            waypoints = []
            for _, location in _landmarks.iterrows():
                waypoint = {
                    "timestamp": datetime.utcfromtimestamp(location["time"]).isoformat(timespec="milliseconds")+"Z",
                    "location": [
                        location["longitude"],
                        location["latitude"]
                    ]
                }
                waypoints.append(waypoint)
            return waypoints

        def match_way_id(way_register, leg_index, step_index):
            return np.nan if np.isnan(leg_index) or np.isnan(step_index) else way_register[int(leg_index)][int(step_index)]

        new_landmarks = pd.DataFrame()

        for i in range(math.ceil(landmarks.shape[0]/1000)):
            if not (i+1) % 10:
                logging.info("Matched {}/{} waypoints.".format((i+1)*1000, landmarks.shape[0]))

            # batch of size 1000
            batch = landmarks.iloc[ i*1000 : min((i+1)*1000, landmarks.shape[0]) ]

            # construct api call
            in_waypoints = json_waypoints(batch)
            in_query = {"waypoints": in_waypoints, "mode": self.config["map_matching_mode"]}
            in_headers = {"content-type": "application/json"}

            # get response data
            response = requests.post(self.api_url, params={"apiKey": self.api_key}, data=json.dumps(in_query), headers=in_headers)
            out_data = response.json()

            # split response
            try:
                out_waypoints = pd.DataFrame(out_data["features"][0]["properties"]["waypoints"])
                out_legs = out_data["features"][0]["properties"]["legs"]
                #out_geometry = out_data["features"][0]["geometry"]["coordinates"]
            except:
                print(out_data)
                raise Exception

            # create a list of list with the osm ways
            way_register = []
            for leg in out_legs:
                out_steps = pd.DataFrame(leg["steps"])
                step_list = out_steps["osm_way_id"].to_list()
                way_register.append(step_list)

            # rewrite coordinates and add way id to batch
            batch.loc[:, ("longitude", "latitude")] = out_waypoints.loc[:, "location"].to_list()
            try:
                way_ids = [match_way_id(way_register, row["leg_index"], row["step_index"]) for _, row in out_waypoints.iterrows()]
                batch.insert(0, "way_id", way_ids)
            except:
                print(batch)
                print(out_waypoints)
                print(out_waypoints[out_waypoints["step_index"].isna()])
                print(out_steps)
                print(way_register)
                raise Exception

            batch.dropna(subset=["way_id"], inplace=True)
            new_landmarks = pd.concat([new_landmarks, batch], ignore_index=True)
        
        return new_landmarks

    def _add_segments(self, ways):
        logging.info("Adding way segments to segment database.")

        def interpolate(loc_a, loc_b, n_segments):
            lat_a, lon_a = loc_a
            lat_b, lon_b = loc_b

            locs = []
            for n in range(n_segments+1):
                lat_ab = (lat_b-lat_a) * n/n_segments + lat_a
                lon_ab = (lon_b-lon_a) * n/n_segments + lon_a
                locs.append([lat_ab, lon_ab])

            return locs

        def get_ids(way_id, n_ids):
            way_id = int(way_id)
            n_ids = int(n_ids)

            digits = math.ceil(math.log10(n_ids))
            base = way_id * (10**digits)

            return list(range(base, base+n_ids))

        def get_segments(way_id, max_segment_length_m):

            overpass_url = "http://overpass-api.de/api/interpreter"
            overpass_query = "[out:json];way({});(._;>;);out;".format(way_id)

            # overpass osm_id search
            success = False
            while not success:
                try:
                    response = requests.get(overpass_url, params={'data': overpass_query})
                    data = response.json()
                    success = True
                except:
                    pass
            
            # get all nodes of way
            nodes = pd.DataFrame(data["elements"][:-1]).set_index(["id"])
            node_ids = data["elements"][-1]["nodes"]
            
            # create segments
            segments = []
            for i, node_id in enumerate(node_ids):

                # skip first node
                if i == 0:
                    continue

                node = nodes.loc[node_id]
                lat, lon = node[["lat", "lon"]]
                node_0 = nodes.loc[node_ids[i-1]]
                lat_0, lon_0 = node_0[["lat", "lon"]]

                # calculate distance and number of subsegments
                distance = hs.haversine((lat_0, lon_0), (lat, lon), unit=hs.Unit.METERS)
                n_subsegments = math.ceil(distance/max_segment_length_m)
                subnodes = interpolate([lat_0, lon_0], [lat, lon], n_subsegments)
                
                for i_subsegment in range(n_subsegments):
                    lat_a, lon_a = subnodes[i_subsegment]
                    lat_b, lon_b = subnodes[i_subsegment+1]

                    # append every segment
                    segments.append(pd.Series([way_id, lat_a, lon_a, lat_b, lon_b, distance/n_subsegments]))
            
            segments = pd.DataFrame(segments)
            segments.columns = ["way_id", "latitude_a", "longitude_a", "latitude_b", "longitude_b", "length"]
            segments.insert(1, "segment_id", get_ids(way_id, segments.shape[0]))
            segments["way_id"] = segments["way_id"].astype("int")
            
            return segments

        # get existing ways from .csv
        existing_ways = pd.read_csv(self.segments, usecols=["way_id"]).drop_duplicates()["way_id"].to_list() if self.segments.exists() else []

        for way in ways:
            # skip existing ways
            if way in existing_ways:
                continue
            else:
                existing_ways.append(way)

            # get segments
            segments = get_segments(way, self.config["max_segment_length_m"])

            # dump to .csv file
            new = not self.segments.exists()
            segments.to_csv(self.segments, mode="a", index=False, header=new)

    def _segment_matching(self, waypoints):
        logging.info("Matching waypoints to segments.")

        segments = pd.read_csv("/studienarbeit/data/segments/10m.csv", index_col="way_id")

        # segment matching for every waypoint
        segment_ids = []
        for i, datapoint in waypoints.iterrows():
            if not (i+1) % 1000:
                logging.info("Matched {}/{} waypoints.".format(i+1, waypoints.shape[0]))

            # all segments with the given id
            way_segments = segments.loc[datapoint["way_id"]]
            
            if type(way_segments) == pd.Series:
                segment_ids.append(way_segments["segment_id"])
            else:
                segment_id = None
                # calculate distances
                for _, way_segment in way_segments.iterrows():

                    # distance to a
                    distance_a = hs.haversine((way_segment["latitude_a"], way_segment["longitude_a"]), (datapoint["latitude"], datapoint["longitude"]), unit=hs.Unit.METERS)
                    if (distance_a - way_segment["length"]) < self.config["segment_match_tolerance_m"]:
                        distance_b = hs.haversine((way_segment["latitude_b"], way_segment["longitude_b"]), (datapoint["latitude"], datapoint["longitude"]), unit=hs.Unit.METERS)
                    
                        # "total" distance
                        distance = distance_a + distance_b - way_segment["length"]
                        if distance < self.config["segment_match_tolerance_m"]:
                            segment_id = way_segment["segment_id"]
                            break
                
                if segment_id == None:
                    # calculate distances to all segments
                    for _, segment in segments.iterrows():

                        # distance to a and b
                        seg_distance_a = hs.haversine((segment["latitude_a"], segment["longitude_a"]), (datapoint["latitude"], datapoint["longitude"]), unit=hs.Unit.METERS)
                        if (seg_distance_a - segment["length"]) < self.config["segment_match_tolerance_m"]:
                            seg_distance_b = hs.haversine((segment["latitude_b"], segment["longitude_b"]), (datapoint["latitude"], datapoint["longitude"]), unit=hs.Unit.METERS)
                            
                            # "total" distance
                            seg_distance = seg_distance_a + seg_distance_b - segment["length"]
                            if seg_distance < self.config["segment_match_tolerance_m"]:
                                segment_id = segment["segment_id"]
                                break
                
                segment_ids.append(segment_id)
        
        waypoints.insert(1, "segment_id", segment_ids)
        waypoints.drop(columns=["latitude", "longitude"], inplace=True)
        waypoints.dropna(subset=["segment_id"], inplace=True)
        return waypoints
    
    def _route_and_track_matching(self, waypoints):
        logging.info("Matching waypoints to route and track.")

        # get table of all recordings
        # go through all .json files in "data/raw"
        meta = []
        for track_json in sorted([f for f in self.raw.glob("*.json")]):
            with open(track_json, "r") as f:
                track_meta = json.load(f)

            meta.append(pd.Series([int(track_meta["name"]), track_meta["date"], track_meta["start_time"], track_meta["end_time"]]))

        meta = pd.DataFrame(meta)
        meta.columns = ["track_id", "date", "start_time", "end_time"]

        # define route id based on date
        route_id_list = meta["date"].drop_duplicates().to_list()
        meta_route_ids = [route_id_list.index(row["date"]) for _, row in meta.iterrows()]
        meta.insert(0, "route_id", meta_route_ids)
        meta.drop(columns="date", inplace=True)

        # go through every waypoint and get route id and track id
        route_ids = []
        track_ids = []
        for i, waypoint in waypoints.iterrows():
            if not (i+1) % 10000:
                logging.info("Matched {}/{} waypoints.".format(i+1, waypoints.shape[0]))

            track = meta[(waypoint["time"] >= meta["start_time"]) & (waypoint["time"] <= (meta["end_time"]+1))]

            if not track.empty:
                route_ids.append(track.iloc[0]["route_id"])
                track_ids.append(track.iloc[0]["track_id"])
            else:
                print(waypoint["time"])
                print(waypoint)

        # insert the ids in the dataframe
        waypoints.insert(0, "track_id", track_ids)
        waypoints.insert(0, "route_id", route_ids)
        waypoints.drop(columns=["time"], inplace=True)
        return waypoints


def main():
    p = Processor()#output_dir=Path("/studienarbeit/data/output/000") )
    p.create_landmarks()
    p.create_waypoints()


if __name__=="__main__":
    main()